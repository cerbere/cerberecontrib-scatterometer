# -*- coding: utf-8 -*-
"""
:mod:`~cerbere.dataset` class for CFOSAT scatterometer L2A and L2B netcdf files.
"""
from collections import OrderedDict
import datetime

import numpy as np
import xarray as xr


from cerberecontrib_scatterometer.dataset.knmil2ncdataset import KNMIL2NCDataset

DEFAULT_TIME_UNITS = 'seconds since 2010-01-01 00:00:00'


class CFOSCATL2NCDataset(KNMIL2NCDataset):
    """dataset class for CFOSAT scatterometer L2A and L2B netcdf files
    """
    def _open(self, **kwargs):
        super(CFOSCATL2NCDataset, self)._open(**kwargs, use_cftime=False)

        # fix and reshape time
        try:
            times = self._std_dataset['time'].values
        except ValueError:
            # bug in some CFOSAT products
            dst = xr.open_dataset(
                self.url, mask_and_scale=False, use_cftime=False
            )
            times = dst['row_time'].values
        if self.url is not None:
            default = (
                (times == b'0000-00-00T00:00:00Z')
                | (times == b'****-**-**T**:**:**Z')
            )
            times[default] = b'0000-01-01T00:00:00Z'
            values = np.ma.masked_where(
                default,
                times,
                copy=False
            ).astype(np.datetime64)
            self._std_dataset['time'].values = values

            dims = (
                self._std_dataset.dims['cell'],
                self._std_dataset.dims['row'],
            )
            times = self._std_dataset['time'].values
            times = np.ma.resize(times, dims).T
            self._std_dataset = self._std_dataset.drop(['time'])
            self._std_dataset = self._std_dataset.assign_coords(
                {'time': xr.DataArray(
                    name='time',
                    data=times,
                    dims=('row', 'cell')
                    )}
            )
            self._std_dataset['time'].encoding['units'] = DEFAULT_TIME_UNITS


        # assign coordinates
        self._std_dataset = self._std_dataset.set_coords(
            ['lat', 'lon', 'time'])

        # fix attributes, add some CF attributes
        for k, v in self._std_dataset.variables.items():
            if 'default_value' in v.attrs:
                v.encoding['_FillValue'] = v.attrs.pop('default_value')
            if 'valid_min' in v.attrs:
                v.attrs.pop('valid_min')
            if 'valid_max' in v.attrs:
                v.attrs.pop('valid_max')
            if k not in self._std_dataset.coords:
                v.encoding['coordinates'] = 'time lat lon'
