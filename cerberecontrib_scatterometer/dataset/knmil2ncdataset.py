# -*- coding: utf-8 -*-
"""
:mod:`~cerbere.dataset` class for KNMI OSI SAF scatterometer netcdf files
"""
from cerbere.dataset.ncdataset import NCDataset

import numpy as np
import scipy.interpolate


class KNMIL2NCDataset(NCDataset):
    """Mapper class for KNMI netcdf files
    """

    def __init__(self, *args,  **kwargs):
        super(KNMIL2NCDataset, self).__init__(
            *args,
            dim_matching={
                'time': 'time',
                'numrows': 'row',
                'NUMROWS': 'row',
                'numcells': 'cell',
                'NUMCELLS': 'cell',
                'NUMAMBIGS': 'solutions',
                'NUMVIEWS': 'views'
            },
            field_matching={
                'row_time': 'time',
                'wvc_lat': 'lat',
                'lat': 'lat',
                'lon': 'lon',
                'wvc_lon': 'lon'
            },
            **kwargs
        )


class FilledKNMIL2NCDataset(NCDataset):
    """Mapper class for KNMI netcdf files, filling undefined lat/lon
    """
    def __init__(self, *args,  **kwargs):
        super(FilledKNMIL2NCDataset, self).__init__(*args, **kwargs)

        rlons = np.radians(self.get_lon())
        lonx = self.interpolate_missing_pixels(
            np.ma.cos(rlons), method='cubic')
        lony = self.interpolate_missing_pixels(
            np.ma.sin(rlons), method='cubic')

        self._std_dataset['lon'][:] = np.degrees(np.arctan2(lony, lonx))
        self._std_dataset['lat'][:] = self.interpolate_missing_pixels(
            self.get_lat(), method='cubic')

    @classmethod
    def interpolate_missing_pixels(
            cls,
            data: np.ma.array,
            method: str = 'nearest'):
        """
        Interpolate a masked array of values

        Args:
            data: a 2D data array
            method: interpolation method, one of 'nearest', 'linear', 'cubic'.
        Returns:
             the data with missing values interpolated
        """
        h, w = data.shape[:2]
        xx, yy = np.meshgrid(np.arange(w), np.arange(h))

        mask = data.mask
        known_x = xx[~mask]
        known_y = yy[~mask]
        known_v = data[~mask]
        missing_x = xx[mask]
        missing_y = yy[mask]

        interp_values = scipy.interpolate.griddata(
            (known_x, known_y), known_v, (missing_x, missing_y),
            method=method, fill_value=np.nan
        )

        data[mask] = interp_values

        data = np.ma.fix_invalid(data)

        mask = data.mask
        known_x = xx[~mask]
        known_y = yy[~mask]
        known_v = data[~mask]
        missing_x = xx[mask]
        missing_y = yy[mask]

        interp_values = scipy.interpolate.griddata(
            (known_x, known_y), known_v, (missing_x, missing_y),
            method='nearest'
        )

        data[mask] = interp_values

        return data
