# -*- coding: utf-8 -*-
"""
Dataset class for CFOSAT SCAT L1B files
"""
import copy
from collections import OrderedDict
from dateutil.parser import parse
import datetime

import numpy as np
import xarray as xr

from cerbere.dataset.ncdataset import NCDataset


TIME_REFERENCE = '2010-01-01 00:00:00'
DEFAULT_TIME_UNITS = 'seconds since ' + TIME_REFERENCE


class CFOSCATL1BNCDataset(NCDataset):

    def __init__(self, *args, **kwargs):
        field_matching = {
            'frame_time': 'time',
            'cell_lon': 'lon',
            'cell_lat': 'lat',
        }
        dim_matching = {
            'frame': 'row',
            'frame_pulse': 'cell',
            'frame_slice': 'slice'
        }
        super(CFOSCATL1BNCDataset, self).__init__(
            *args,
            field_matching=field_matching,
            dim_matching=dim_matching,
            drop_variables=['frame_time'],
            **kwargs
        )

    def _open(self, **kwargs):
        super(CFOSCATL1BNCDataset, self)._open(**kwargs)

        # fix and reshape time
        if self.url is not None:
            timearr = xr.open_dataarray(
                self.url,
                drop_variables=(
                    list(self._std_dataset.variables.keys()) +
                    ['cell_lon', 'cell_lat']),
                mask_and_scale=False
            )
            day, millisec = np.array_str(timearr[0].astype(str).data).split(':')
            start = np.datetime64(
                datetime.datetime.strptime(day, '%Y%m%dT%H%M%S')
            ) + np.array(int(millisec), dtype='timedelta64[ms]')
            times = start + (self._std_dataset['orbit_time'] * 1000000).astype(
                'timedelta64[us]'
            ).to_masked_array()
            dims = (
                self._std_dataset.dims['cell'],
                self._std_dataset.dims['row'],
            )
            times = np.ma.resize(times, dims).T
            self._std_dataset = self._std_dataset.assign_coords(
                {'time': xr.DataArray(
                    name='time',
                    data=times,
                    dims=('row', 'cell')
                    )
                }
            )
            self._std_dataset['time'].encoding['units'] = DEFAULT_TIME_UNITS

        # assign coordinates
        self._std_dataset = self._std_dataset.set_coords(
            ['lat', 'lon', 'time'])

        # fix attributes, add some CF attributes
        for k, v in self._std_dataset.variables.items():
            if 'default_value' in v.attrs:
                v.encoding['_FillValue'] = v.attrs.pop('default_value')
            if 'valid_min' in v.attrs:
                v.attrs.pop('valid_min')
            if 'valid_max' in v.attrs:
                v.attrs.pop('valid_max')
            if k not in self._std_dataset.coords:
                v.encoding['coordinates'] = 'time lat lon'

    def get_values(self, fieldname, *args, **kwargs):
        values = super(CFOSCATL1BNCDataset, self).get_values(
            fieldname, *args, **kwargs
        )
        # mask lat/lon both equal to 0. (bad fill value)
        altercoord = {'lat': 'lon', 'lon': 'lat'}
        if fieldname in altercoord.keys():
            altervalues = super(CFOSCATL1BNCDataset, self).get_values(
                altercoord[fieldname], *args, **kwargs
            )
            values = np.ma.masked_where(
                ((values == 0.) & (altervalues == 0.)),
                values,
                copy=False
            )

        return values

    def get_collection_id(self):
        """return the identifier of the product collection"""
        return 'NSOAS-CFO-SCAT-L1B'
