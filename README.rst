============================
cerberecontrib-scatterometer
============================

Cerbere Datasset classes for various scatterometer products, including:
  * KNMIL2NCDataset : OSI SAF L2 products from KNMI
  * CFOSCATL2NCDataset : CWWIC L2A et L2B datasets for CFOSAT/SCAT
  * CFOSCATL1BNCDataset : CWWIC L1B dataset for CFOSAT/SCAT

.. code-block:: python

  # example : opening a CFOSAT SCAT L2A product
  import cerbere

  dst = cerbere.open_dataset(
      '/tmp/CFO_OPER_SCA_L2A____F_20200611T054340_20200611T071825.nc',
      'CFOSCATL2NCDataset',
  )

