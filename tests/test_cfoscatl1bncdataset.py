"""

Test class for cerbere GRIB dataset

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker


class CFOSCATL1BNCDatasetChecker(Checker, unittest.TestCase):
    """Test class for GRIB files"""

    def __init__(self, methodName="runTest"):
        super(CFOSCATL1BNCDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'CFOSCATL1BNCDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "CFO_OPER_SCA_L1B____F_20190609T000952_20190609T014331.nc"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
