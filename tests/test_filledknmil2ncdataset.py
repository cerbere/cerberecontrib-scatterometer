"""

Test class for KNMI SCAT L2 datasets files with lat/lon interpolation

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker

import cerbere


class FilledKNMIL2NCDatasetChecker(Checker, unittest.TestCase):
    """Test class for KNMI SCAT L2 datasets files with lat/lon interpolation"""

    def __init__(self, methodName="runTest"):
        super(FilledKNMIL2NCDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'FilledKNMIL2NCDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "hscat_20131016_084128_hy_2a__10289_o_250_ovw_l2.bufr.nc"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"

    def test_open_dataset(self):
        dst = self.open_file()

        # verify lat/lon interpolated
        flon = dst.get_lon()
        self.assertEqual(flon.count(), flon.size)


