"""

Test class for cerbere CFOSAT SCAT L2 datasets

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker


class CFOSCATL2NCDatasetChecker(Checker, unittest.TestCase):
    """Test class for CFOSAT SCAT L2 datasets files"""

    def __init__(self, methodName="runTest"):
        super(CFOSCATL2NCDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'CFOSCATL2NCDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "CFO_OPER_SCA_L2A____F_20200407T115657_20200407T133001.nc"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
