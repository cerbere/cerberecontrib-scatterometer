# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the Cerbere extension for various scatterometer datasets.
'''

requires = [
   # 'cerbere>=2.0.0',
]

setup(
    name='cerberecontrib-scatterometer',
    version='1.0',
    url='',
    license='GPLv3',
    authors='Jean-François Piollé',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for scatterometer products',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    entry_points={
        'cerbere.plugins': [
            'CFOSCATL1BNCDataset = cerberecontrib_scatterometer.dataset.cfoscatl1bncdataset:CFOSCATL1BNCDataset',
            'CFOSCATL2NCDataset = cerberecontrib_scatterometer.dataset.cfoscatl2ncdataset:CFOSCATL2NCDataset',
            'KNMIL2NCDataset = cerberecontrib_scatterometer.dataset.knmil2ncdataset:KNMIL2NCDataset',
            'FilledKNMIL2NCDataset = '
            'cerberecontrib_scatterometer.dataset.knmil2ncdataset'
            ':FilledKNMIL2NCDataset',
        ]
    },
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
)
